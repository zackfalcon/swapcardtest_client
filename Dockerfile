# base image
FROM node:9.6.1

RUN mkdir /usr/src/swapcardtest_client
WORKDIR /usr/src/swapcardtest_client

ENV PATH /usr/src/swapcardtest_client/node_modules/.bin:$PATH

COPY package.json /usr/src/swapcardtest_client/package.json

EXPOSE 3000

RUN npm install

CMD npm start
