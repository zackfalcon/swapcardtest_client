import React, { Component } from 'react';
import { ApolloProvider } from "react-apollo";
import { ApolloClient, InMemoryCache } from "apollo-boost";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { LocaleProvider } from 'antd';
import frFR from 'antd/lib/locale-provider/fr_FR';

import AuthLayout from './view/auth/authLayout';
import LoginLayout from './view/auth/login';
import RegisterLayout from './view/auth/register';

import MainLayout from './view/main/mainLayout';
import createBrowserHistory from "history/createBrowserHistory";

import { split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

import './App.css';

const history = createBrowserHistory()
const httpLink = new HttpLink({
  uri: 'http://localhost:4000/graphql'
});

const wsLink = new WebSocketLink({
  uri: 'ws://localhost:5000/',
  options: {
    reconnect: true
  }
});

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLink,
);

const client = new ApolloClient({
  link, cache: new InMemoryCache()
});

class App extends Component {
  render() {
    return (
      <LocaleProvider locale={frFR}>
        <ApolloProvider client={client}>
          <Router history={history}>
            <Switch>
              <Route exact path="/auth" component={AuthLayout} />
              <Route exact path="/auth/login" component={LoginLayout} />
              <Route exact path="/auth/register" component={RegisterLayout} />
              <Route path="/" component={MainLayout} />
            </Switch>
          </Router>
        </ApolloProvider>
      </LocaleProvider>
    );
  }
}

export default App;
