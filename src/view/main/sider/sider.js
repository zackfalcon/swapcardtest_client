import React from 'react';

import { Layout, Menu, Icon, Modal, Input } from 'antd';
import { withRouter } from 'react-router-dom';
import { graphql, compose } from "react-apollo";

import gql from "graphql-tag";
import logoSwapcard from '../../../assets/logo.png';

import './sider.less';



const { Sider } = Layout;
const SubMenu = Menu.SubMenu;

class Siders extends React.PureComponent {
  constructor(props) {
    super(props)
    this.logoutUser = this.logoutUser.bind(this);
    this.addChannel = this.addChannel.bind(this);
  }
  state = { 
    visible: false,
    name: '',
    owner: this.props.userName,
  }

  addChannel = () => {
    this.setState({
      visible: true,
    });
  }

  handleOk = async () => {
    console.log(this.props);
    this.setState({
      visible: false,
    });
    const { name, owner } = this.state

    const response = await this.props.addNewChannel({
        variables: { name, owner }
    })
    if (!response.data.bool) {
      console.log("Erreur encoutered... bad name or owner detected");
    }
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }

  onChangeChannelName = (e) => {
    this.setState({ name: e.target.value });
  }

  logoutUser() {
    localStorage.clear();
    this.props.history.push('/auth');
    window.location.reload();
  }

  render() {

    return (
      <Sider width={220} >
        <Menu
          mode="inline"
          theme="light"
          defaultSelectedKeys={['myaccount']}
          defaultOpenKeys={['sub1']}
          style={{ height: '100%', borderRight: '5px solid #133456' }}
        >
          <center>
            <img alt="logo_swapcard" src={logoSwapcard} style={{ width: 150, textAlign: 'center', margin: 20, marginBottom: 40 }} />
          </center>
          <SubMenu
            key="sub2"
            title={<span><Icon type="inbox" style={{ fontSize: 20 }} /><span style={{ fontSize: 20 }}>Channels</span></span>}
          >
            {this.props.data.channels && this.props.data.channels.map((data) => {
              return (<Menu.Item key={data.name} style={{ fontSize: 16 }}><Icon type="message" style={{ fontSize: 16 }} />{data.name}</Menu.Item>)
            })}

          </SubMenu>
          <Menu.Item key="addChannel" onClick={this.addChannel}><Icon type="plus-circle" theme="outlined" />Créer un channel</Menu.Item>
          <Menu.Item key="logoutUser" onClick={this.logoutUser}><Icon type="logout" theme="outlined" />Déconnexion</Menu.Item>
        </Menu>
        <Modal
          title="Créer un channel"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Input
            placeholder="Entrer le nom d'un nouveau channel"
            prefix={<Icon type="message" style={{ color: 'rgba(0,0,0,.25)' }} />}
            value={this.state.name}
            onChange={this.onChangeChannelName}
            ref={node => this.channelNameInput = node}
          />
        </Modal>
                <Modal
          title="Créer un channel"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Input
            placeholder="Entrer le nom d'un nouveau channel"
            prefix={<Icon type="message" style={{ color: 'rgba(0,0,0,.25)' }} />}
            value={this.state.name}
            onChange={this.onChangeChannelName}
            ref={node => this.channelNameInput = node}
          />
        </Modal>
      </Sider>
    )
  }
}

const query = 
  gql`
    query {
      channels {
        id
        name
        owner
      }
    }
  `;

const mutation = 
  gql`
  mutation addNewChannel($name: String!, $owner: String!) {
    addNewChannel(name: $name, owner: $owner) {
      bool
      token
      message
    }
  }
  `
;

const SiderRouter = withRouter(Siders);
const SiderApollo = compose(
  graphql(query), graphql(mutation, {name: "addNewChannel"}))(SiderRouter)

export default SiderApollo;
