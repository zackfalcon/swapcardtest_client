import React from 'react';

import { Form, Input, Button, Row, Alert } from 'antd';
import logoSwapcard from '../../../assets/logo.png';

import { Link, withRouter } from 'react-router-dom';
import { graphql } from "react-apollo";
import gql from "graphql-tag";

import Styles from './login.less';

const FormItem = Form.Item;

const isEmpty = (value) => {
    return (
        value === undefined ||
        value === null ||
        (typeof value === 'object' && Object.keys(value).length === 0) ||
        (typeof value === 'string' && value.trim().length === 0)
    );
}

class Login extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            errors_server: null,
            validate: null
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit = async () => {
        let stop = false;
        this.setState({
            errors_server: null
        })
        if (isEmpty(this.state.username) || isEmpty(this.state.password)) {
            this.setState({
                errors_server: "Des champs sont encore vide."
            })
            stop = true;
        }
        if (!stop) {
            const { username, password } = this.state
            const response = await this.props.mutate({
                variables: { username, password }
            })
            const { bool, token, message } = response.data.login;
            if (bool) {
                localStorage.setItem('jwtToken', token);
                localStorage.setItem('userName', username);
                console.log(localStorage.getItem('jwtToken'));
                this.props.history.push({
                    pathname: '/',
                    search: '',
                    state: { userName: username }
                  })
            } else {
                this.setState({
                    errors_server: message
                })
            }
        }
    }



    render() {
        return (
            <div className={Styles.form}>
                <div className={Styles.logo}>
                    <img alt="logo_swapcard" src={logoSwapcard} />
                </div>

                {this.state.errors_server && (<Alert message={this.state.errors_server} type="error" className={Styles.errorAlert}></Alert>)}

                <form onSubmit={this.handleSubmit}>
                    <FormItem hasFeedback>
                        <Input type="text" placeholder="Nom d'utilisateur" name="username"
                            onChange={this.handleInputChange} value={this.state.username}

                        />
                    </FormItem>

                    <FormItem hasFeedback>
                        <Input type="password" placeholder="Mot de passe" name="password"
                            onChange={this.handleInputChange} value={this.state.password} />
                    </FormItem>
                    <Row>
                        <Button type="primary" className={Styles.btn} onClick={this.onSubmit}>
                            Connexion
                        </Button>
                    </Row>
                    <Link to="/auth"><span className={Styles.backSmoothBtn}>Retour en arrière</span></Link>
                    <Link to="/auth/register"><span className={Styles.registerBtn}>Je n'ai pas de compte.</span></Link>
                </form>
            </div>
        );
    }
}

const query = graphql(
    gql`
      mutation login($username: String!, $password: String!) {
        login(username: $username, password: $password) {
            bool
            token
            message
        }
      }
    `
);

const LoginRouter = withRouter(Login);
const LoginApollo = query(LoginRouter);

export default (withRouter(LoginApollo))