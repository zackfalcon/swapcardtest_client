import React from 'react';

import { Form, Input, Button, Row, Alert } from 'antd';
import { Link, withRouter } from 'react-router-dom';

import logoSwapcard from '../../../assets/logo.png';
import Styles from './register.less';

import { graphql } from "react-apollo";
import gql from "graphql-tag";

const FormItem = Form.Item;

const isEmpty = (value) => {
    return (
        value === undefined ||
        value === null ||
        (typeof value === 'object' && Object.keys(value).length === 0) ||
        (typeof value === 'string' && value.trim().length === 0)
    );
}

class Register extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            email: '',
            username: '',
            name: '',
            password: '',
            password_confirm: '',
            errors_server: null,
            validate: null
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }



    onSubmit = async () => {
        let stop = false;
        this.setState({
            errors_server: null
        })
        if (isEmpty(this.state.name) || isEmpty(this.state.email) || isEmpty(this.state.username) || isEmpty(this.state.password) || isEmpty(this.state.password_confirm)) {
            this.setState({
                errors_server: "Des champs sont encore vide."
            })
            stop = true;
        }
        if (this.state.password !== this.state.password_confirm) {
            this.setState({
                errors_server: "Les deux mots de passe ne sont pas similaire"
            })
            stop = true;
        }
        if (!stop) {
            const { email, username, name, password } = this.state
            const response = await this.props.mutate({
                variables: { email, username, name, password }
            })
            const { bool, message } = response.data.signUp;
            if (bool) {
                this.setState({
                    validate: "Inscription finalisé : Connectez vous dès maintenant !"
                })
            } else {
                this.setState({
                    errors_server: message
                })
            }
        }
    }

    
    render() {
        return (
            <div className={Styles.form}>
                <div className={Styles.logo}>
                    <img alt="logo_swapcard" src={logoSwapcard} />
                </div>
                {this.state.validate && (<Alert message={this.state.validate} type="success" className={Styles.validateAlert}></Alert>)}
                {this.state.errors_server && (<Alert message={this.state.errors_server} type="error" className={Styles.errorAlert}></Alert>)}

                <FormItem hasFeedback>
                    <Input type="text" placeholder="Nom" name="name"
                        onChange={this.handleInputChange} value={this.state.name}

                    />
                </FormItem>
                <FormItem hasFeedback>
                    <Input type="text" placeholder="Nom d'utilisateur" name="username"
                        onChange={this.handleInputChange} value={this.state.username}

                    />
                </FormItem>

                <FormItem hasFeedback>
                    <Input type="text" placeholder="Email" name="email"
                        onChange={this.handleInputChange} value={this.state.email}

                    />
                </FormItem>

                <FormItem hasFeedback>
                    <Input type="password" placeholder="Mot de passe" name="password"
                        onChange={this.handleInputChange} value={this.state.password} />
                </FormItem>
                <FormItem hasFeedback>
                    <Input type="password" placeholder="Re - Mot de passe" name="password_confirm"
                        onChange={this.handleInputChange} value={this.state.password_confirm} />
                </FormItem>
                <Row>
                    <Button type="primary" className={Styles.btn} onClick={this.onSubmit}>
                        S'INSCRIRE
                        </Button>
                </Row>
                <Link to="/auth"><span className={Styles.backSmoothBtn}>Retour en arrière</span></Link>
                <Link to="/auth/login"><span className={Styles.loginBtn}>J'ai déjà un compte.</span></Link>

            </div>
        );
    }
}

const query = graphql(
    gql`
      mutation signUp($email: String!, $username: String!, $name: String!, $password: String!) {
        signUp(email: $email, username: $username, name: $name, password: $password) {
            bool
            token
            message     
        }
      }
    `
);

const RegisterRouter = withRouter(Register);
const RegisterApollo = query(RegisterRouter);

export default RegisterApollo