import React from 'react';
import Slider from './sider';

import { withRouter } from 'react-router-dom';
import { Layout, Button, Icon, Input, List, Avatar } from 'antd';

const { Footer } = Layout;

const data = [
  {
    name: 'Zakaria',
    message: 'Salut les mecs je suis bg',
    owner: true
  },
  {
    name: 'Test',
    message: 'Hello les guys',
    owner: false
  },
];

class MainLayout extends React.PureComponent {
  constructor(props) {
    super(props)
    this.onChangeText = this.onChangeText.bind(this);
    this.emitEmpty = this.emitEmpty.bind(this);
    this.addNewMessage = this.addNewMessage.bind(this);
  }

  state = {
    collapsed: false,
    message: '',
    userName: '',
  };

  emitEmpty = () => {
    this.setState({ message: '' });
  }

  onChangeText = (e) => {
    this.setState({ message: e.target.value });
  }

  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({ collapsed });
  }

  addNewMessage = () => {
    console.log("gilet jaune");
  }

  componentDidMount() {
    if (!localStorage.getItem('jwtToken')) {
      this.props.history.push('/auth');
    } else {
      if (this.props.location.state) {
        this.setState({
          userName: this.props.location.state.userName
        })
      }
    }
  }


  render() {
    const { message } = this.state;
    const suffix = message ? <Icon type="close-circle" onClick={this.emitEmpty} /> : null;

    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Layout>
          <Slider userName={this.state.userName} />
          <Layout style={{ padding: '0 24px 24px' }}>

            <div style={{ padding: 24, background: '#fff', minHeight: 650, position: 'relative' }}>
              <div style={{
                color: 'rgb(18, 52, 86)', border: '2px dashed rgb(18, 52, 86)', marginBottom: 50, padding: 10, textAlign: 'center', fontWeight: 'bold', fontSize: 30, textTransform: 'uppercase', width: '70%', marginRight: 'auto',
                marginLeft: 'auto'
              }}>
                Channel : Excalibur
                <span style={{ fontSize: 20, display: 'block', color: '#5acd88' }}>Owner: Zakaria</span>
              </div>
              <List
                itemLayout="horizontal"
                dataSource={data}
                renderItem={item => (
                  <List.Item>
                    <List.Item.Meta
                      avatar={<Avatar style={{ backgroundColor: '#5acd88', fontWeight: 'bold' }}>{item.name[0]}</Avatar>}
                title={<div>{item.owner && <Icon type="crown" style={{fontSize: 20, color: '#5acd88'}}/>} <span style={{fontWeight: 'bold'}}>{item.name}</span></div>}
                      description={item.message}
                    />
                  </List.Item>
                )}
              />
              <Layout  style={{
                    position: 'absolute',
                    bottom: 20,
                    width: '90%',
                    marginRight: 'auto',
                    marginLeft: 'auto',
                    left: 0,
                    right: 0,
                    display: 'block',
                    background: 'white'
                  }}>
                <Input
                  placeholder="Entrer un message..."
                  prefix={<Icon type="message" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  suffix={suffix}
                  value={message}
                  onChange={this.onChangeText}
                  ref={node => this.messageInput = node}
                  style={{marginBottom: 15}}
                />
                <Button type="primary" onClick={this.addNewMessage} style={{background: '#5acd88', border: '1px solid #5acd88', textTransform: 'uppercase', letterSpacing: '2px', fontSize: '12px'}}>Envoyer...</Button>
              </Layout>
            </div>
            <Footer style={{ textAlign: 'center' }} >
              Swapcard Chat - Copyright 2018
            </Footer>
          </Layout>
        </Layout>
      </Layout>);
  }
}

export default withRouter(MainLayout);
